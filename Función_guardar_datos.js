function estudiantesguardar() {
    letcedula = document.getElementById("cedula").value;
    let apellidos = document.getElementById("apellidos").value;
    let nombres = document.getElementById("nombres").value;
    let direccion = document.getElementById("direccion").value;
    let semestre = document.getElementById("semestre").value;
    let paralelo = document.getElementById("paralelo").value;
    let correo = document.getElementById("correo").value;

    //validamos datos
    if (cedula.length == 0 || apellidos.length == 0 || nombres.length == 0 || direccion.length == 0 || semestre.length == 0 || paralelo.length == 0 || correo.length == 0) {
        alert("Campos son obligatorios");
    }
    //hacemos el llamado de los datos para guardarlos
    else {
        let datos = {
            'cedula': cedula,
            'apellidos': apellidos,
            'nombres': apellidos,
            'direccion': direccion,
            'semestre': semestre,
            'paralelo': paralelo,
            'correo': correo
        };
        localStorage.setItem('usuario', JSON.stringify(datos));
    }
}